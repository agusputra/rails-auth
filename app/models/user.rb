class User < ActiveRecord::Base
  has_many :authentications, :dependent => :destroy
  accepts_nested_attributes_for :authentications

  authenticates_with_sorcery!

  validates :password, length: {minimum: 3}, if: -> { new_record? || changes["password"] }
  validates :password, confirmation: true, if: -> { new_record? || changes["password"] }
  validates :password_confirmation, presence: true, if: -> { new_record? || changes["password"] }

  validates :email, uniqueness: true

  authenticates_with_sorcery! do |config|
    config.authentications_class = Authentication
  end
end